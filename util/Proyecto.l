%{
#include "Proyecto.tab.h"
#include <string.h>
#include <stdlib.h>

//host, status, address, hostname, extraports, extrareasons, port, state, service

/*
> -------- CLOSE2
</PORTS> - CLOSE1
/> ------- CLOSE3
*/

%}

any .
fc "-->"
fh "?>"
opentag "<"
close2 ">"
close3 "/>"
value [^\"\t\n]+
word [^ \t\n","";"">"]+
Digito [0-9]
delim [ \t\n]
espacio " "
ws {delim}

%option yylineno

%%
"<!--"{any}+{fc}					{}
"<?"{any}+{fh}						{ yylval.head = strdup(yytext); return HEAD; }
{close2}								{ yylval.tag = strdup(yytext);  return CLOSE2; }
{close3}								{ yylval.tag = strdup(yytext);  return CLOSE3; }
{opentag}"/"{word}{close2}		{ yylval.tag = strdup(yytext);  return CLOSE1; }
"product="							{ yylval.tag = strdup(yytext); return PRODUCT;}
{opentag}"host"					{ yylval.tag = strdup(yytext);  return HOST; }
{opentag}"hostname"           { yylval.tag = strdup(yytext);  return HOSTNAME;}
{opentag}"status"					{ yylval.tag = strdup(yytext);  return STATUS; }
{opentag}"address"				{ yylval.tag = strdup(yytext);  return ADDR; }
{opentag}"ports"{close2}      { yylval.tag = strdup(yytext);  return PORTS; }
{opentag}"port"               { yylval.tag = strdup(yytext);  return PORT; }
{opentag}"state"              { yylval.tag = strdup(yytext);  return STATE; }
{opentag}"service"            { yylval.tag = strdup(yytext);  return SERVICE; }
{opentag}"extraports"			{ yylval.tag = strdup(yytext);  return EXTRAP; }
{opentag}"extrareasons"       { yylval.tag = strdup(yytext);  return EXTRAR; }
{opentag}{word}">"				{ yylval.tag = strdup(yytext);  return CTAG; } 
{opentag}{word}					{ yylval.tag = strdup(yytext);  return OTAG; }
{word}"="							{ yylval.campo = strdup(yytext); return CAMPO; }
\"{value}\" 						{ yylval.valor = strdup(yytext); return VALOR; }
{ws} 				  					{}
