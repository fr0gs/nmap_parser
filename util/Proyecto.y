%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "util/util.h"

#define MAXHOST 128
#define MAXPORT 64
#define MAXPROD 64

/*
Importante : para hacer búsquedas en la base de datos de exploits se buscarán por el nombre del producto únicamente
y no por el producto + versión, ya que el número de versión muchas veces es engañoso, y por nombre de producto la 
búsqueda arroja más resultados.
*/

typedef struct{
		char * family; //TCP o UDP
		int port; //puerto asociado al servicio
		char * nombre; //nombre del servicio
} service;

typedef struct{
		char * ip; //ip del host
		char * hostname; //nombre dns del host 
		char * so; //SO del host
		char * upDown; //si esta UP o DOWN
		int portOpen; //numero de puertos abiertos en el host
		int portFiltered; //numero de puertos filtrados en el host
		int portClosed; //numero de puertos cerrados en el host
  	service servicios[MAXPORT]; //servicios detectados en ese host
} host;

typedef struct {
	char * nombre_producto;
	int count;
	struct strlist * producto_v;
} t_productos;


host data[MAXHOST]; //Asumimos que no habra escaneos por ahora de mas de MAXHOST hosts.
t_productos productos[MAXPROD];

int nHost = -1;
int nPort = -1;
int nProd = 0;
int hostUp = 0;
int hostDown = 0;
int total_open_ports = 0; /*Los puertos que detecta cerrados no los detecta como tcp o udp*/
int total_closed_ports = 0;
int total_filtered_ports = 0;
int total_tcp_open_ports = 0;
int total_tcp_filtered_ports = 0;
int total_udp_open_ports = 0;
int total_udp_filtered_ports = 0;
int percent_host_up = 0;
int percent_host_down = 0;
char * tcp_udp_aux;


void yyerror (char const *s){
	printf ("Error: Fallo al procesar el archivo\n");
	exit(0);
}

void printError (char *err){
	printf ("Error: %s\n", err);
	exit(0);
}

%}
%union{
	char * valor;
	char * head;
	char * campo;
	char * tag;
}

%token <valor> VALOR
%token <head> HEAD
%token <tag> HOST
%token <tag> HOSTNAME
%token <tag> STATUS
%token <tag> ADDR
%token <tag> PORTS
%token <tag> PORT
%token <tag> STATE
%token <tag> SERVICE
%token <tag> EXTRAP
%token <tag> EXTRAR
%token <tag> CTAG
%token <tag> OTAG
%token <tag> CLOSE1
%token <tag> CLOSE2
%token <tag> CLOSE3
%token <campo> CAMPO
%token <campo> PRODUCT

%type <tag> inicio
%type <tag> n0
%type <tag> n1
%type <tag> ex
%type <tag> hos
%type <tag> status
%type <tag> addr
%type <tag> ports
%type <tag> port
%type <tag> state
%type <tag> service
%type <tag> extrar
%type <tag> hostn


%start inicio

%%
inicio: HEAD n0 {} | HEAD inicio {}

n0: {} 
	| CTAG n0 CLOSE1 n0 {} 
	| HOST hos CLOSE2 n0 CLOSE1 n0 {}
	| HOSTNAME hostn CLOSE3 n0 {}
	| STATUS status CLOSE3 n0 {}
	| ADDR addr CLOSE3 n0 {}	
	| PORTS ports CLOSE1 n0 { 
		nPort = -1; //Esto tiene que ejecutarse forzosamente al final, una vez recorridos todos los puertos se resetea para el siguiente host. 
	 }
	| OTAG n1 CLOSE2 n0 CLOSE1 n0 {}
	| OTAG n1 CLOSE3 n0 {};

n1: {}
	| CAMPO VALOR n1 {
		//control host
		if (!strcmp($1,"ostype=")) data[nHost].so = xstrdup($2);
		/*
		//control port
		if (!strcmp($1,"protocol="))
			if (!strcmp($2,"\"tcp\"")) data[nHost].servicios[nPort++].family = TCP;
			else  data[nHost].servicios[nPort++].family = UDP;
		if (!strcmp($1,"portid=")) data[nHost].servicios[nPort].port = xstrdup($2);
		if (!strcmp($1,"name=")) data[nHost].servicios[nPort].nombre = xstrdup($2);
		*/
	};

hos: CAMPO VALOR CAMPO VALOR {
	if (strcmp($1, "starttime=") || (strcmp($3, "endtime="))) printError("Sintaxis incorrecta: host");
};

hostn: CAMPO VALOR CAMPO VALOR {
	if (!strcmp($1,"name=") && (!strcmp($3,"type="))) data[nHost].hostname = xstrdup(strtok($2, "\""));
	else printError("Sintaxis incorrecta: hostname");
};


status: CAMPO VALOR CAMPO VALOR {
	if (!strcmp($1, "state=") && (!strcmp($3, "reason="))){
		nHost++;
		if (!strcmp($2,"\"up\"")) {
			hostUp++;
			data[nHost].upDown = "UP";
		}
		else if (!strcmp($2,"\"down\"")) {
			hostDown++;
			data[nHost].upDown = "DOWN";
		}
	}
	else printError("Sintaxis incorrecta: status");
};

addr: CAMPO VALOR CAMPO VALOR {
	if (!strcmp($1, "addr=") && (!strcmp($3, "addrtype="))){
		data[nHost].ip = xstrdup($2);
		nPort = -1;
	}
	else printError("Sintaxis incorrecta: address");
};
	
ex: CAMPO VALOR CAMPO VALOR {
	if (!strcmp($1,"state=") && (!strcmp($3,"count="))) {
		char * aux = xstrdup(strtok($4, "\""));
		total_closed_ports += atoi(aux);
		data[nHost].portClosed = atoi(aux);
	}
	else printError("Sintaxis incorrecta: extraports");
};

extrar: EXTRAR CAMPO VALOR CAMPO VALOR {
	if (!strcmp($2, "reason=") && (!strcmp($4, "count="))){
		
	}
	else printError("Sintaxis incorrecta: extrareasons");
}; 

service: {}
	| CAMPO VALOR service {}
	| CAMPO VALOR PRODUCT VALOR service {
		if (!strcmp($1,"name=")) {
			char * aux = xstrdup(strtok($2, "\""));
			data[nHost].servicios[nPort].nombre = aux;
			if (str_exist(aux) == -1) {
				productos[nProd].nombre_producto = xstrdup(aux);
				productos[nProd].count++;
				nProd++;
			}
			else productos[nProd].count++;

			char * aux1 = xstrdup(strtok($4, "\""));
			int pos = str_exist(aux);
			if (strlist_have_element(productos[pos].producto_v, aux1) == 0) strlist_append_string(productos[pos].producto_v, aux1);
		}
	};

state: STATE CAMPO VALOR CAMPO VALOR CAMPO VALOR CLOSE3 SERVICE service CLOSE3 CLOSE1{
		if (!strcmp($2, "state=") && (!strcmp($4, "reason=")) && (!strcmp($6, "reason_ttl="))){
			if (!strcmp($3,"\"open\"")){
				tcp_udp_aux = xstrdup(strtok($3, "\""));
				data[nHost].portOpen++;
				total_open_ports++;
			}
		   if (!strcmp($3, "\"filtered\"")) {
				tcp_udp_aux = xstrdup(strtok($3, "\""));
				data[nHost].portFiltered++;
				total_filtered_ports++;
			}		
		}
		else printError("Sintaxis incorrecta: state");
};

port: CAMPO VALOR CAMPO VALOR CLOSE2 state {
	if (!strcmp($1, "protocol=") && (!strcmp($3, "portid="))){
		nPort++;
		if (!strcmp($2, "\"tcp\"")) {
			char * aux = xstrdup(strtok($4, "\""));
			data[nHost].servicios[nPort].family = xstrdup("tcp");
			data[nHost].servicios[nPort].port = atoi(aux);

			if (!strcmp(tcp_udp_aux, "open")) total_tcp_open_ports++;
			else if (!strcmp(tcp_udp_aux, "filtered")) total_tcp_filtered_ports++;
		}
		if (!strcmp($2, "\"udp\"")) {
			char * aux = xstrdup(strtok($4, "\""));
			data[nHost].servicios[nPort++].family = xstrdup("udp");
			data[nHost].servicios[nPort].port = atoi(aux);

			if (!strcmp(tcp_udp_aux, "open")) total_udp_open_ports++;
			else if (!strcmp(tcp_udp_aux, "filtered")) total_udp_filtered_ports++;
		}
	}
	else printError("Sintaxis incorrecta: port");
};

ports: {}
	| EXTRAP ex CLOSE2 extrar CLOSE3 CLOSE1 ports {};
	| PORT port ports {};

%%

/*Devuelve -1 si no existe el nombre en la lista de productos, y la posicion si existe*/
int 
str_exist (char * aux)
{
	int i;
	for (i=0; i<nProd; i++){
		if (!strcmp(productos[i].nombre_producto, aux)) return i;
	}
	return -1;
}

void inicialize() {
	int i, j;

	for (i=0; i<MAXHOST; i++) {
		data[i].so = "Desconocido";
		data[i].portOpen = 0;
		data[i].portFiltered = 0;
		data[i].portClosed = 0;	
	}

	for (i=0; i<MAXPROD; i++) {
		productos[i].count = 0;
		productos[i].producto_v = strlist_new();
		}
}

/*Cambia los strings de la forma "abc bc" a "abc+bc" */
char *
str_replace(const char * str) 
{
	int len = strlen(str);
	char * aux;
	char * tokenizable = xstrdup(str);
	char * ret = (char*)malloc(sizeof(char)*len+1);

	aux = strtok(tokenizable, " ");
	strcat(ret, aux);
	strcat(ret, "+");
	while((aux = (char*)strtok(NULL," ")) != NULL){
		strcat(ret, aux);
		strcat(ret, "+"); //Asi añade '+' a cada token
	}
	ret[len] = '\0';
	return ret;
}

/*Descarga el html con nombre web.html para la búsqueda de cada uno de los servicios*/
void 
descargar_html(const char * aux, void * data)
{
	char * ret = xstrdup("&filter_exploit_text=&filter_author=&filter_platform=0&filter_type=0&filter_lang_id=0&filter_port=&filter_osvdb=&filter_cve=");
	char * ret2 = xstrdup("http://www.exploit-db.com/search/?action=search&filter_page=1&filter_description=");	
	char * arg[5]; 
	char * aux_s = xstrdup(aux);
	char * str_encoded = str_replace(aux);

	int len = strlen(str_encoded);

	arg[0] = xstrdup("wget");
	arg[1] = xstrdup("-O");
	arg[2] = xstrdup("web.html");
	arg[3] = (char*)xmalloc(sizeof(char)*strlen(ret2)+len+strlen(ret)+1); //Si se vuelven a necesitar comillas cambiarlo por +3
	arg[4] = NULL;	

	strcpy(arg[3], ret2);
	strcat(arg[3], str_encoded);
	strcat(arg[3], ret);
	strcat(arg[3], "\0");
	
	pid_t pid = fork();	
	if(pid != 0){ //Si el pid es distinto de cero significa que es el padre.
		waitpid(pid, NULL, NULL);
		fflush(stdout);
		fprintf(stdout, "::::: Producto %s :::::\n", aux_s);
		system("./html_parse < web.html");
		system("rm -f salida.txt");
		//sleep(1);
		system("rm -f web.html");
	}
	else {
		int fd = open("salida.txt", O_WRONLY | O_CREAT | O_TRUNC, 0755); 
		if (fd == -1) { 
			perror("fallo en open"); exit(EXIT_FAILURE); 
		}
		dup2(fd, STDOUT_FILENO);
		dup2(fd, STDERR_FILENO);
		execv("/usr/bin/wget", arg);	
	}
	free(arg[3]);
}


void 
buscar_exploits()
{
	int i;
	for (i=0; i<nProd; i++){
		strlist_walk(productos[i].producto_v, NULL, (void *)descargar_html);
	}
}



void imprimir() {
	int i, j;

	for (i=0;i<=nHost;i++) {
		printf("Host nº %d %s\n",i, data[i].upDown);
		printf("Ip: %s  SO: %s\n",data[i].ip, data[i].so);
		printf("Hostname: %s\n", data[i].hostname);
		printf("Puertos abiertos/cerrados/filtrados: %d/%d/%d\n",data[i].portOpen, data[i].portClosed, data[i].portFiltered);
		printf("\n\n");
	}
	
	printf("Número de hosts totales %d  Host Up/Down: %d/%d\n",nHost+1, hostUp, hostDown);
	float perc = (hostUp/(nHost+1))*100;
	printf("\"%\" de host up / down: %.2f/%.2f\n", perc, 100.0-perc);
	printf("Número total de puertos tcp abiertos %d\n", total_tcp_open_ports);
	printf("Número total de puertos tcp filtrados %d\n", total_tcp_filtered_ports);
	printf("Número total de puertos udp abiertos %d\n", total_udp_open_ports);
	printf("Número total de puertos udp filtrados %d\n", total_udp_filtered_ports);
	printf("Número de puertos totales cerrados %d\n", total_closed_ports);	

	printf("\n\n");

	printf("*** Lista de servicios y lista de versiones asociada ***\n");
	for (i=0; i<nProd; i++){
		printf("\t- Nombre del producto: %s\n", productos[i].nombre_producto);
		printf("\t- Número de puertos abiertos para el servicio %s: %d\n", productos[i].nombre_producto, productos[i].count);
		printf("\t-- Lista de versiones para el servicio %s\n:", productos[i].nombre_producto);
		strlist_debug(productos[i].producto_v);
		printf("\n");
	}
}

int main (int argc, char *argv[]) {
	inicialize();
	yyparse();
	printf("\nAnálisis completo\nResultado:\n\n");
	imprimir();
	buscar_exploits();
	return 0;	
}
