all:
	flex Proyecto.l
	bison -o Proyecto.tab.c Proyecto.y -yd
	gcc -c util/util.c
	gcc -g -o Proyecto lex.yy.c Proyecto.tab.c util.o -lfl -ly
	rm lex.yy.c Proyecto.tab.* util.o

	sleep 1

	flex html_parse.l
	gcc -c util/util.c
	gcc -g -o html_parse lex.yy.c util.o -ll
	rm lex.yy.c util.o

clean:
	rm html_parse Proyecto

	
