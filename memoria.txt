*********************************************************

Objetivos: 
	Como ya se indica en la propuesta de proyecto escrita en el foro, se busca crear un analizador/parser de ficheros de salida en formato
	XML generados por el programa de mapeo y análisis de red nmap. A partir de los datos extraídos se realizarán varios recuentos de puertos,
	servicios, etc... y se buscará en internet exploits para esos servicios detectados.

Funcionamiento:
	Una vez realizado el escaneo de uno o varios host con el nmap y obtener su salida en un fichero XML, el primer analizador parsea el fichero
	y obtiene los diversos parámetros y estadísticas que se indicaron en la propuesta, y además una lista de servicios detectados junto con otra
	sub-lista cada uno con las distintas versiones de ese servicio/producto. 
	Tras ese primer análisis, para cada uno de las distintas versiones detectadas en total, se hará una petición al repositorio de exploits www.exploit-db.com
	buscando los exploits disponibles.
	El fichero descargado de nombre "web.html", que será sobreescrito cada vez que se busquen exploits para una version, se pasará como entrada al segundo analizador,
	"html_parse" , que se encargará simplemente de buscar en el HTML todos los exploits que existan para esa version y sus links asociados y mostrarlos por pantalla.
	De modo que una ejecución normal sería:
			<INFORMACION RELACIONADA CON EL ANÁLISIS DE LOS DATOS>
				
			Producto detectado: http
							 versiones: Apache httpd ; Nginx; etc..
			
			Producto detectado: ftp
							 versiones: ProFTPD ; vsftp ; etc..
			
			Apache httpd :
				 exploit1 -> link1
				 exploit2 -> link2 
				 (...)
			Nginx :
				 exploit1 -> link1
				 exploit2 -> link2 
				 (...)
			ProFTPD: 
				 exploit1 -> link1
				 exploit2 -> link2 
				 (...)

Control de errores:
	Detectamos los siguientes errores en los tags del fichero XML de salida del nmap. No detectamos errores en el HTML porque no se hace un tratamiento
	exhaustivo de los tokens.

	<host, <hostname, <status, <address, <ports>, <port, <state, <service, <extraports, <extrareasons    

Otras anotaciones:
	Para evitar implementar una lista de punteros a cadenas al almacenar los productos y sus distintas versiones, utilizaremos la libreria "util.h",
	escrita en 2011 por un antiguo alumno de la facultad (Gonzalo J. Carracedo) y licenciada bajo GPL.
 
