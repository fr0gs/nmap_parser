%{
#include <string.h>
#include <stdlib.h>

//<td class="list_explot_description">
//											<a  href="http://www.exploit-db.com/exploits/6094">Debian OpenSSH Remote SELinux Privilege Elevation Exploit (auth)</a>

char * str[32];
int i = 0;

%}

any .
fc "-->"
fh "?>"
comilla "\""
barra "/"
opentag "<"
closetag ">"
value [^\"\t\n]+
word [^ \t\n","";"">"]+
Digito [0-9]
delim [ \t\n]
espacio " "
ws {delim}

%option yylineno

%%


{opentag}"td"{espacio}"class="{comilla}"list_explot_description"{comilla}{closetag}{ws}+{opentag}"a  href="{comilla}{value}{comilla}{closetag}{value}{ws}*{opentag}{barra}"a"{closetag} { str[i] = strdup(yytext); i++;}

{ws} 				  					{}
.										{}
%%
int main()
{	
	int j,k;
	yylex();
	
	for (j=0; j<i; j++){
		char * str_aux = strtok(str[j], "<");
		char * str_link_exploit;
		char * str_link_exploit_final;

		str_aux = (char*)strtok(NULL, ">");
	
		str_link_exploit = strdup(str_aux);

		str_aux = strtok(NULL, "<");
		char * str_nombre = strdup(str_aux);

		str_link_exploit = strtok(str_link_exploit, "\"");
		str_link_exploit = strtok(NULL, "\"");

		fprintf(stdout, "[==] Nombre del exploit: %s  ----- Link al exploit: %s\n", str_nombre, str_link_exploit);
	
	}
	fprintf(stdout, "--------------------------------------------------------------------------\n");
	return 0;
}
